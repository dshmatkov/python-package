import unittest

from labwork2.utils import mxrange


class MxrangeTest(unittest.TestCase):

    def test_generating_list(self):
        testing = [x for x in mxrange(100)]
        checking = [x for x in xrange(100)]
        self.assertEqual(testing, checking, 'Fail simple list generation')

    def test_generating_from_to_list(self):
        testing = [x for x in mxrange(4, 100)]
        checking = [x for x in mxrange(4, 100)]
        self.assertEqual(testing, checking, 'Fail not so simple list gen')

    def test_generating_custom_list(self):
        testing = [x for x in mxrange(5, 103, 4)]
        checking = [x for x in xrange(5, 103, 4)]
        self.assertEqual(testing, checking, 'Fail not simple list gen')
