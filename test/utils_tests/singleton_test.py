import unittest
from labwork2.utils import Singleton, DecorSingleton


class SimpleSingleton(Singleton):

    def __init__(self, a):
        super(SimpleSingleton, self).__init__()
        self.a = a


class SingletonTest(unittest.TestCase):

    def test_singleton(self):
        a = SimpleSingleton(1)
        b = SimpleSingleton(2)
        self.assertTrue(a is b)
