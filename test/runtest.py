import unittest

from handler_tests import JsonicTest, FileSortTest
from metaclasses_tests import ModelsTest, MetaClassTest
from mycollections_tests import NvectTest, LfuncTest, FilcollectionTest, \
    DefdictTest
from utils_tests import MxrangeTest, SingletonTest

loader = unittest.TestLoader()
suite = unittest.TestSuite((
    loader.loadTestsFromTestCase(SingletonTest),
    loader.loadTestsFromTestCase(MxrangeTest),
    loader.loadTestsFromTestCase(NvectTest),
    loader.loadTestsFromTestCase(LfuncTest),
    loader.loadTestsFromTestCase(DefdictTest),
    loader.loadTestsFromTestCase(FilcollectionTest),
    loader.loadTestsFromTestCase(FileSortTest),
    loader.loadTestsFromTestCase(JsonicTest),
    loader.loadTestsFromTestCase(MetaClassTest),
    loader.loadTestsFromTestCase(ModelsTest)
))

runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)
