import unittest
from labwork2.metaclasses import MetaClass


class MetaClassTest(unittest.TestCase):

    def test_create_new_object(self):
        with open('test.txt', 'w') as f:
            f.write(
                'a 1\nb asdf\nc 123\n'
            )
        obj = MetaClass('ClassName', [object], 'test.txt')
        self.assertEqual(obj.__name__, 'ClassName',
                         'Bad ClassName')
        self.assertEqual(obj.a, '1')
        self.assertEqual(obj.b, 'asdf')
        self.assertEqual(obj.c, '123')
