import unittest
from labwork2.metaclasses import models


class Model():
    __metaclass__ = models.ModelCreator

    int_field = models.Integer()
    string_field = models.String()
    float_field = models.Float()


class Model2():
    __metaclass__ = models.ModelCreator

    int_field = models.Integer(1)
    string_field = models.String('1')
    float_field = models.Float(100.0)


class ModelsTest(unittest.TestCase):

    def test_object_creating_with_defaults(self):
        obj = Model()
        self.assertEqual(obj.int_field, 0)
        self.assertEqual(obj.string_field, '')
        self.assertEqual(obj.float_field, float(0))

    def test_object_creating(self):
        obj = Model2()
        self.assertEqual(obj.int_field, 1)
        self.assertEqual(obj.string_field, '1')
        self.assertEqual(obj.float_field, float(100))

