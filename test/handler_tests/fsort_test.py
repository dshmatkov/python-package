import unittest

from labwork2.handlers import file_merge_sort


class FileSortTest(unittest.TestCase):

    _file_name = 'test.txt'

    def setUp(self):
        with open(self._file_name, 'w') as f:
            for x in reversed(xrange(10000)):
                f.write(str(x) + '\n')

    def test_sort(self):
        file_merge_sort(self._file_name)
        prev = -10000
        with open(self._file_name, 'r') as f:
            current = f.readline().split('\n')[0]
            while current != '':
                current = float(current)
                self.assertTrue(current >= prev, 'Sort is not working')
                current = f.readline().split('\n')[0]

    def wrong_test_sort(self):
        file_merge_sort(self._file_name)
        prev = -10000
        with open(self._file_name, 'r') as f:
            current = f.readline().split('\n')[0]
            while current != '':
                current = float(current)
                self.assertFalse(current >= prev, 'Sort is not working')
                current = f.readline().split('\n')[0]
