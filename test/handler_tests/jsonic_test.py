import json
import unittest

from labwork2.handlers import to_json
from labwork2.handlers.jsonic import from_json


class JsonicTest(unittest.TestCase):

    _dict = {
        '1': [1, 2, 3],
        '2': False,
        '3': 'hello, World!',
        '4': 123
    }

    def test_to_json(self):
        self.assertTrue(json.dumps(self._dict) == to_json(self._dict),
                        'Something wrong')

    def test_from_json(self):
        s = json.dumps(self._dict)
        self.assertEqual(from_json(s), self._dict)
