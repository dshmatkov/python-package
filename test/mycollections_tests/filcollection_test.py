import unittest
from labwork2.mycollections import FilteringCollection


class FilcollectionTest(unittest.TestCase):

    def test_iteration(self):
        c = FilteringCollection([0, 1, 2])
        i = 0
        for item in c:
            self.assertEqual(i % 3, item)
            i += 1
            if i > 100:
                break

    def test_filtering(self):
        c = FilteringCollection([1, 2, 3, 4, 5, 6, 7])
        new_c = c.filter(
            lambda x: x % 2 == 0
        )
        i = len(new_c)
        j = 0
        data_set = []
        for item in new_c:
            data_set.append(item)
            j += 1
            if j == i:
                break
        self.assertEqual(data_set, [2, 4, 6])
