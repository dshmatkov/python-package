import unittest
from labwork2.mycollections import NVect


class NvectTest(unittest.TestCase):

    def test_nvect(self):
        v = NVect([1, 2, 3])

        self.assertEqual(v[1], 2)
        self.assertEqual(len(v), 3)

        v1 = NVect([3, 2, 1])

        v = v + v1
        self.assertEqual(v[0], 4)
        self.assertEqual(v[1], 4)
        self.assertEqual(v[2], 4)
