import unittest
from labwork2.mycollections import LinearFunc


class LfuncTest(unittest.TestCase):

    def test_linear_func(self):
        f = LinearFunc(1, 1)
        self.assertEqual(f(1), 2)
        f1 = LinearFunc(2, 2)
        f2 = f + f1
        self.assertEqual(f2(1), 6)
        self.assertEqual((f*2)(1), 4)
        self.assertEqual(str(f), 'f(x) = 1*x+1')
