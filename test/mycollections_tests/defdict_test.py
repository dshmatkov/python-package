import unittest
from labwork2.mycollections import DefaultDict


class DefdictTest(unittest.TestCase):

    def test_default_dict_usual_access(self):
        d = DefaultDict()
        d['a'] = 1
        self.assertEqual(d['a'], 1)

    def test_realized_dict_access(self):
        d = DefaultDict()
        self.assertEqual(type(d['a']), DefaultDict)
