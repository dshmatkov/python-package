def mxrange(start, stop=None, step=1):
    if stop == None:
        stop = start
        start = 0

    if step == 0:
        raise ValueError('Step must be not zero.')

    while (stop - start + 0.0) / step > 0:
        yield start
        start += step
