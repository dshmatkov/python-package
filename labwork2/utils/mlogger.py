class MLogger(object):
    __log = ''

    __default_format = '\n'.join([
        'Call: {procedure}',
        '   Args: {args}',
        '   KWArgs: {kwargs}',
        '   Result: {result}',
        'End call.\n\n'
    ])

    def _logger(self, func):
        def runner(*args, **kwargs):
            result = func(*args, **kwargs)
            log = self.__default_format.format(
                procedure=str(func),
                args=args,
                kwargs=kwargs,
                result=result
            )
            self.__log += log
            return result
        return runner

    def __getattribute__(self, item):
        attr = super(MLogger, self).__getattribute__(item)
        if item == '_logger' or not callable(attr):
            return attr
        else:
            return self._logger(attr)

    def __str__(self):
        return self.__log


class A(MLogger):
    def f(self, x):
        return x


a = A()
a.f(1)
print a