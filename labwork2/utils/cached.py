__cache = {}


def cached(func):
    def decorator(*args, **kwargs):
        string_func = str(func)
        string_args = str(args) + ',' + str(kwargs)
        if string_args not in __cache[string_func]:
            result = func(*args, **kwargs)
            __cache[string_func][string_args] = result
        else:
            result = __cache[string_func][string_args]
        return result

    if str(func) not in __cache:
        __cache[str(func)] = {}
    return decorator
