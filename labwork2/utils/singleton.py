class Singleton(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super(cls.__class__, cls).__new__(cls)
        return cls._instance


class DecorSingleton(object):
    _instance = None
    _cls = None

    def __init__(self, cls):
        self._cls = cls

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = self._cls.__new__(self._cls)
            self._instance.__init__(*args, **kwargs)
        return self._instance
