class MetaClass(type):
    def __new__(mcs, class_name, base_classes, filename):
        attrs = {}
        with open(filename, 'r') as f:
            temp = f.readline()
            while temp != '':
                pair = temp.split('\n')[0].split(' ')
                attrs[pair[0]] = pair[1]
                temp = f.readline()
        return type(class_name, tuple(base_classes), attrs)
