

class Integer(object):
    def __init__(self, default=0):
        if not isinstance(default, int):
            raise ValueError('Wrong type.')
        self.value = default


class String(object):
    def __init__(self, default=''):
        if not isinstance(default, str):
            raise ValueError('Wrong type.')
        self.value = default


class Float(object):
    def __init__(self, default=0.0):
        if not isinstance(default, float):
            raise ValueError('Wrong type.')
        self.value = default


class ModelCreator(type):

    _SUPPORTED_CLASSES = (
        Integer,
        String,
        Float
    )

    def __call__(cls, **kwargs):
        for key, value in kwargs.iteritems():
            attr = getattr(cls, key)
            if isinstance(attr, cls._SUPPORTED_CLASSES) and\
                    isinstance(value, type(attr.value)):
                setattr(cls, key, value)
            else:
                raise ValueError('Attribute {key} does not match type.'
                                 .format(key=key))
        attrs = dir(cls)
        for attr_name in attrs:
            attr = getattr(cls, attr_name)
            if not attr_name.startswith('__') and\
                    isinstance(attr, cls._SUPPORTED_CLASSES):
                setattr(cls, attr_name, attr.value)
        return cls.__new__(cls)
