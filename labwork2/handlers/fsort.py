import tempfile


def __merge(file, f1, l1, f2, l2):
    i = 0
    j = 0
    num_1 = None
    if l1 > 0:
        tmp = f1.readline()
        num_1 = float(tmp)
    num_2 = None
    if l2 > 0:
        tmp = f2.readline()
        num_2 = float(tmp)
    while i < l1 and j < l2:
        if num_1 < num_2:
            file.write(str(num_1) + '\n')
            i += 1
            if i < l1:
                num_1 = float(f1.readline())
        else:
            file.write(str(num_2) + '\n')
            j += 1
            if j < l2:
                num_2 = float(f2.readline())
    while i < l1:
        file.write(str(num_1) + '\n')
        i += 1
        if i < l1:
            num_1 = float(f1.readline())
    while j < l2:
        file.write(str(num_2) + '\n')
        j += 1
        if j < l2:
            num_2 = float(f2.readline())
    file.seek(0)
    f1.seek(0)
    f2.seek(0)


def __merge_sort(data_file, length):
    if length <= 1:
        return
    tf_1 = tempfile.TemporaryFile(mode='r+w')
    tf_2 = tempfile.TemporaryFile(mode='r+w')
    m = length // 2
    i = 0
    for str in data_file:
        if str == '':
            continue
        if i < m:
            tf_1.write(str)
        else:
            tf_2.write(str)
        i += 1
    data_file.seek(0)
    tf_1.seek(0)
    tf_2.seek(0)
    len_1 = m
    len_2 = length - m
    __merge_sort(tf_1, len_1)
    __merge_sort(tf_2, len_2)
    __merge(data_file, tf_1, len_1, tf_2, len_2)


# in each string one number
def file_merge_sort(filename):
    f = open(filename, 'r+w')
    tfile = tempfile.TemporaryFile(mode='r+w')
    length = 0
    for line in f:
        num = float(line)
        tfile.write(str(num) + '\n')
        length += 1
    f.seek(0)
    tfile.seek(0)
    __merge_sort(tfile, length)
    for line in tfile:
        f.write(line)
