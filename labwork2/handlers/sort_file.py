from fsort import file_merge_sort
import argparse


def execute_from_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help='Print your filename here.')
    args = parser.parse_args()

    file_merge_sort(args.filename)
