import regex

def __handle_key(key):
    if type(key) == float or type(key) == int:
        return '"' + str(key) + '"'
    elif type(key) == str:
        return '"' + key + '"'
    else:
        raise TypeError('Keys must be a string')


def to_json(obj):
    result = None
    if type(obj) == dict:
        result = '{'
        for key, value in obj.iteritems():
            result += __handle_key(key) + ': ' + to_json(value) + ', '
        result = result[:len(result) - 2]
        result += '}'
        return result
    elif type(obj) == list:
        result = '['
        for item in obj:
            result += to_json(item) + ', '
        result = result[:len(result) - 2]
        result += ']'
        return result
    elif type(obj) == set:
        return to_json(list(obj))
    elif type(obj) == float or type(obj) == int:
        return str(obj)
    elif type(obj) == bool:
        return str(obj).lower()
    elif type(obj) == str:
        return '"' + obj + '"'
    elif obj in None:
        return 'null'
    else:
        raise TypeError('Non serializable in JSON type')


_object_pattern = r'(?P<object>\{([^\}\{]*|(?&object))+\})'
_sequence_pattern = r'(?P<sequence>\[([^\]\[]*|(?&sequence))+\])'
_string_pattern = r'[\"].*?[\"]'
_int_pattern = r'[+-]?[\d]+'
_float_pattern = r'[+-]?[\d]+\.[\d]+'
_boolean_pattern = r'true|false'
_none_pattern = r'null'


_value_pattern = r'(?P<value>' + '|'.join((
    _object_pattern,
    _sequence_pattern,
    _int_pattern,
    _float_pattern,
    _string_pattern,
    _boolean_pattern,
    _none_pattern
)) + ')'


_item_pattern = '(?P<key>{key}){delimiter}{value}'.format(
    delimiter=r'\s*:\s*',
    key=_string_pattern,
    value=_value_pattern
)


def _parse_sequence(json_sequence):
    matches = regex.finditer(_value_pattern, json_sequence[1:-1])
    return [from_json(match.group('value')) for match in matches]


def _parse_dict(json_object):
    matches = regex.finditer(_item_pattern, json_object[1: -1])
    dictionary = dict()
    for match in matches:
        key = match.group('key').lstrip('\'\"').rstrip('\'\"')
        value = from_json(match.group('value'))
        dictionary[key] = value
    return dictionary


from_json_converters = {
    _object_pattern: _parse_dict,
    _sequence_pattern: _parse_sequence,
    _int_pattern: int,
    _float_pattern: float,
    _string_pattern: lambda json_str: json_str.lstrip('\"').rstrip('\"'),
    _boolean_pattern: lambda json_str: json_str == 'true',
    _none_pattern: lambda json_str: None,
}


def from_json(json_string):
    for type_pattern, parse_func in from_json_converters.items():
        if regex.match(type_pattern, json_string, flags=regex.DOTALL):
            return parse_func(json_string)
    else:
        raise ValueError('Json syntax error')
