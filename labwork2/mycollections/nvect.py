import math


class NVect(object):

    def __init__(self, values):
        self.__coordinates = []
        for value in values:
            self.__coordinates.append(float(value))

    def __str__(self):
        result = '('
        for coordinate in self.__coordinates:
            result += str(coordinate) + ', '
        result = result[:len(result) - 2]
        result += ')'
        return result

    def __raise_type_error(self, x):
        raise TypeError('{type} is not valid type.'.format(
            type=str(type(x))
        ))

    def __check_valid(self, other):
        if type(other) is not NVect:
            self.__raise_type_error(other)
        if len(other.__coordinates) != len(self.__coordinates):
            raise ValueError('Vectors has different count of dimensions')

    def __cmp__(self, other):
        if type(self) != type(other):
            return -1
        if len(self.__coordinates) != len(other.__coordinates):
            return -1
        for index in xrange(len(self.__coordinates)):
            if self.__coordinates[index] != other.__coordinates[index]:
                return -1
        return 0

    def __eq__(self, other):
        if cmp(self, other) == 0:
            return True
        else:
            return False

    def __ne__(self, other):
        return not (a == self)

    def __add__(self, other):
        self.__check_valid(other)
        result = NVect([])
        for index in xrange(len(self.__coordinates)):
            result.__coordinates.append(self.__coordinates[index] + \
                                        other.__coordinates[index])
        return result

    def __sub__(self, other):
        self.__check_valid(other)
        result = NVect([])
        for index in xrange(len(self.__coordinates)):
            result.__coordinates.append(self.__coordinates[index] - \
                                        other.__coordinates[index])
        return result

    def __mul__(self, other):
        if type(other) == float or type(other) == int:
            result = NVect([])
            for i in xrange(len(self.__coordinates)):
                result.__coordinates.append(
                    self.__coordinates[i] * other
                )
            return result
        result = 0
        self.__check_valid(other)
        for i in xrange(len(self.__coordinates)):
            result += self.__coordinates[i] * other.__coordinates[i]

    def __abs__(self):
        result = 0
        for c in self.__coordinates:
            result += c * c
        result = math.sqrt(result)
        return result

    def __iadd__(self, other):
        return self.__add__(other)

    def __isub__(self, other):
        return self.__sub__(other)

    def __imul__(self, other):
        return self.__mul__(other)

    def __len__(self):
        return len(self.__coordinates)

    def __check_index(self, key):
        if type(key) is int:
            if 0 <= key < len(self.__coordinates):
                return key
            elif 0 <= len(self.__coordinates) - key < len(self.__coordinates):
                return len(self.__coordinates) - key
            else:
                raise IndexError('Index out of range')
        else:
            self.__raise_type_error(key)

    def __getitem__(self, key):
        index = self.__check_index(key)
        return self.__coordinates[key]

    def __setitem__(self, key, value):
        index = self.__check_index(key)
        self.__coordinates[key] = float(value)
