class LinearFunc(object):
    __e = 0.00000000000000001

    def __init__(self, k, b):
        if not self.__check_numeric_type(k):
            self.__raise_type_error(k)
        if not self.__check_numeric_type(b):
            self.__raise_type_error(b)
        self.__k = k
        self.__b = b
        self.__func = None

    def __is_zero(self, x):
        if -self.__e < x < self.__e:
            return True
        else:
            return False

    def __is_one(self, x):
        if 1.0 - self.__e < x < 1.0 + self.__e:
            return True
        else:
            return False

    def __check_numeric_type(self, x):
        if not type(x) is int and not type(x) is float:
            return False
        else:
            return True

    def __raise_type_error(self, x):
        raise TypeError('{type} is not valid type.'.format(
            type=str(type(x))
        ))

    def __call__(self, x):
        if type(x) is LinearFunc:
            self.__func = x
            return self
        if not self.__check_numeric_type(x):
            self.__raise_type_error()
        result = 0
        if self.__func is None:
            result = self.__k * x + self.__b
        else:
            result = self.__k * self.__func(x) + b
        return result

    def __add__(self, other):
        if not type(other) is LinearFunc:
            self.__raise_type_error(other)
        return LinearFunc(self.__k + other.__k, self.__b + other.__b)

    def __sub__(self, other):
        if not type(other) is LinearFunc:
            self.__raise_type_error(other)
        return LinearFunc(self.__k - other.__k, self.__b - other.__b)

    def __neg__(self, other):
        return LinearFunc(-self.__k, -self.__b)

    def __mul__(self, c):
        if type(c) is LinearFunc:
            self.__func = c
            return self
        if not self.__check_numeric_type(c):
            self.__raise_type_error(c)
        return LinearFunc(self.__k * c, self.__b * c)

    def __div__(self, c):
        if not self.__check_numeric_type(c):
            self.__raise_type_error(c)
        return LinearFunc(self.__k / c, self.__b / c)

    def __str__(self):
        k, b = self.__coef()
        res_k = ''
        if self.__is_zero(k):
            res_k = ''
        elif self.__is_one(k):
            res_k = 'x'
        elif self.__is_one(-k):
            res_k = '-x'
        else:
            res_k = '{k}*x'.format(k=str(k))
        res_b = ''
        if self.__is_zero(b):
            res_b = ''
        elif b < 0:
            res_b = '-{b}'.format(b=str(-b))
        elif res_k == '':
            res_b = str(b)
        else:
            res_b = '+{b}'.format(b=str(b))
        func = ''
        func = res_k + res_b
        if func == '':
            func = '0'
        return 'f(x) = {func}'.format(func=func)

    def __coef(self):
        if self.__func is None:
            return self.__k, self.__b
        else:
            k, b = self.__func.__coef()
            k = self.__k * k
            b = self.__k * b + self.__b
            return k, b
