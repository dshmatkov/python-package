class FilteringCollection(list):

    def __init__(self, data=[]):
        self.__current = 0
        super(self.__class__, self).__init__(data)

    def __iter__(self):
        return self

    def next(self):
        result = self[self.__current]
        self.__current = (self.__current + 1) % len(self)
        return result

    def filter(self, selector):
        if not callable(selector):
            if selector is True:
                return self
            else:
                return FilteringCollection()
        result = FilteringCollection()
        i = 0
        for item in self:
            if selector(item):
                result.append(item)
            i += 1
            if i == len(self):
                break
        return result
