from setuptools import find_packages, setup

setup(
    name='labwork2',
    version='0.1.0',
    description='Lab works number 2 on python.',
    install_requires=['math', 'regex'],
    author='client',
    packages=find_packages(exclude=['test', '*_tests']),
    scripts=['labwork2/handlers/sort_file.py'],
    entry_points={
        'console_scripts': [
            'sort_file = labwork2.handlers.sort_file:execute_from_command_line'
        ]
    }
)
